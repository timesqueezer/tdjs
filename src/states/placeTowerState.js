import * as PIXI from 'pixi.js';

import app from '../app';
import { getTileUnderCursor, gridToPixel } from '../utils';
import constants from '../constants';
import Tower from '../entities/tower';

export default class PlaceTowerState {
    constructor() {
        this.name = 'placeTower';
        this.highlightedPosition = null;

        this.pointer = app.g.pointerHelper.makePointer();
        this.pointer.tap = this.onPointerTap.bind(this);

        console.log(this.pointer, this.pointer.tap)
    }

    onEnter() {
        this.tileHighlight = new PIXI.Graphics();
        app.stage.addChild(this.tileHighlight);
    }

    onExit() {
        app.stage.removeChild(this.tileHighlight);
    }

    update(delta) {
        this.tileHighlight.clear();
        this.highlightedPosition = null;

        const tileUnderCursor = getTileUnderCursor();
        if (!tileUnderCursor) {
            return;
        }

        this.tileHighlight.lineStyle(0);
        this.tileHighlight.beginFill(0x000000, 0.1);

        const tilePxPos = gridToPixel(tileUnderCursor);
        this.tileHighlight.drawRect(tilePxPos.x, tilePxPos.y, constants.GRID_SIZE, constants.GRID_SIZE);
        this.tileHighlight.endFill();

        this.highlightedPosition = tileUnderCursor;
    };

    onPointerTap() {
        if (this.highlightedPosition) {
            console.log('tapped at ', this.highlightedPosition);
            let tower = new Tower();
            const tilePxPos = gridToPixel(this.highlightedPosition);
            tower.x = tilePxPos.x;
            tower.y = tilePxPos.y;
            tower.place();
            // entityMananger.addEntity(tower);
        }

    }


}
