import PlayState from './playState';
import PlaceTowerState from './placeTowerState';

class StateManager {
    constructor() {
        this.activeState = null;
        this.states = {};
    }

    registerState(stateCls) {
        const newState = new stateCls();
        this.states[newState.name] = newState;
    }

    switchTo(name) {
        console.log('Switching to state [' + name + ']')
        if (this.activeState && this.activeState.onExit) {
            this.activeState.onExit();
        }

        const newState = this.states[name];
        if (newState.onEnter) {
            console.log('yes');
            newState.onEnter();
        }

        this.activeState = this.states[name];
    }

    updateActiveState(delta) {
        this.activeState.update(delta);
    }

    registerStates() {
        this.registerState(PlayState);
        this.registerState(PlaceTowerState);
    }
}

const manager = new StateManager();

export default manager;
