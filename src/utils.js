import app from './app';
import constants from './constants';

function getTileUnderCursor() {
    const mouseposition = app.renderer.plugins.interaction.mouse.global;
    const renderPadding = app.g.renderPadding;

    if ((mouseposition.x <= renderPadding.x || mouseposition.x >= app.screen.width - renderPadding.x) ||
        (mouseposition.y <= renderPadding.y || mouseposition.y >= app.screen.height - renderPadding.y)) {
        return null;
    }

    let tilePos = {};
    tilePos.x = parseInt((mouseposition.x - renderPadding.x) / constants.GRID_SIZE);
    tilePos.y = parseInt((mouseposition.y - renderPadding.y) / constants.GRID_SIZE);

    return tilePos;
}


function gridToPixel(gridPosition) {
    return {
        x: (gridPosition.x * constants.GRID_SIZE) + app.g.renderPadding.x,
        y: (gridPosition.y * constants.GRID_SIZE) + app.g.renderPadding.y
    }
}

function keyboard(keyCode) {
    let key = {};
    key.code = keyCode;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    //The `downHandler`
    key.downHandler = event => {
        if (event.keyCode === key.code) {
            if (key.isUp && key.press) key.press();
            key.isDown = true;
            key.isUp = false;
        }
        event.preventDefault();
    };

    //The `upHandler`
    key.upHandler = event => {
        if (event.keyCode === key.code) {
            if (key.isDown && key.release) key.release();
            key.isDown = false;
            key.isUp = true;
        }
        event.preventDefault();
    };

    //Attach event listeners
    window.addEventListener(
        "keydown", key.downHandler.bind(key), false
    );
    window.addEventListener(
        "keyup", key.upHandler.bind(key), false
    );
    return key;
}

export { getTileUnderCursor, gridToPixel, keyboard };
