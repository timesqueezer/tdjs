import * as PIXI from 'pixi.js';

import app from '../app';

export default class Tower extends PIXI.Sprite {
    constructor() {
        let texture = PIXI.TextureCache['tower1'];
        super(texture);
    }

    place() {
        app.stage.addChild(this);
    }
}
