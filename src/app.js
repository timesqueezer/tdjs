import * as PIXI from 'pixi.js';

import constants from './constants';

const appOptions = {
    antialias: true,
};

const app = new PIXI.Application(appOptions);
app.renderer.backgroundColor = 0xFAFAFA;
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight - constants.UI_HEIGHT);
app.g = {};

export default app;
