import './styles.scss';

import stateManager from './states';
import app from './app';
import constants from './constants';
import { gridToPixel } from './utils';
import PointerHelper from './pointer';

import * as PIXI from 'pixi.js';
import * as ui from './ui.js';

let Application = PIXI.Application,
    Container = PIXI.Container,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Sprite = PIXI.Sprite,
    Rectangle = PIXI.Rectangle;

document.body.appendChild(app.view);

const graphics = new PIXI.Graphics();

/* ASSETS */
import bgImage from '../assets/bg.png';
import markerImage from '../assets/marker.png';
import tower1Image from '../assets/tower1.png';

import testMap from '../assets/maps/test.json';

loader
  .add('bg', bgImage)
  .add('marker', markerImage)
  .add('tower1', tower1Image)
  .on("progress", loadProgressHandler)
  .load(setup);

function loadProgressHandler(loader, resource) {
    console.log('loading: ' + resource.url + resource.name ? (' (' + resource.name + ')') : '');
    console.log('progress: ' + loader.progress + '%');

}

let bg,
    posText,
    sizeText,
    state,
    mouseposition;

let renderPadding;

function setup() {
    /*let bgTexture = TextureCache['bg'];
    bg = new PIXI.extras.TilingSprite(
        bgTexture,
        app.screen.width,
        app.screen.height
    );
    app.stage.addChild(bg);*/

    document.getElementById('placeTowerButton').onclick = function() {
        stateManager.switchTo('placeTower');

    };

    let pointerHelper = new PointerHelper(PIXI, app.renderer.view);
    app.g.pointerHelper = pointerHelper;

    stateManager.registerStates();

    app.ticker.add(function() {
        mouseposition = app.renderer.plugins.interaction.mouse.global;
    });

    if (constants.GRID_DEBUG) {
        sizeText = new PIXI.Text('w: ' + app.screen.width + ' h: ' + app.screen.height);
        sizeText.position.set(10, 10);
        app.stage.addChild(sizeText);

        posText = new PIXI.Text();
        posText.position.set(10, 40);

        app.stage.addChild(posText);

        app.ticker.add(function(delta) {
            posText.text = 'x: ' + mouseposition.x + ' y: ' + mouseposition.y;
        });
    }

    loadMap(testMap);

    app.stage.addChild(graphics);

    stateManager.switchTo('play');

    app.ticker.add(delta => gameLoop(delta))
}

let startMarker,
    endMarker;

function loadMap(map) {
    const requiredWidth = map.size[0] * constants.GRID_SIZE;
    const requiredHeight = map.size[1] * constants.GRID_SIZE;

    renderPadding = {
        x: (app.screen.width - requiredWidth) / 2,
        y: (app.screen.height - requiredHeight) / 2,
    };

    app.g.renderPadding = renderPadding;

    console.log('renderPadding', renderPadding);

    let markerTexture = TextureCache['marker'];
    startMarker = new Sprite(markerTexture);
    endMarker = new Sprite(markerTexture);

    const startMarkerPosition = gridToPixel(map.startMarker);
    const endMarkerPosition = gridToPixel(map.endMarker);
    startMarker.position.set(startMarkerPosition.x, startMarkerPosition.y)
    endMarker.position.set(endMarkerPosition.x, endMarkerPosition.y)

    app.stage.addChild(startMarker);
    app.stage.addChild(endMarker);

    // map outline
    graphics.lineStyle(2, 0x030507, 1);
    graphics.drawRect(renderPadding.x, renderPadding.y, app.screen.width - (renderPadding.x * 2), app.screen.height - (renderPadding.y * 2));

    // padding outline
    graphics.lineStyle(1, 0x030507, .5);
    graphics.drawRect(constants.PADDING, constants.PADDING, app.screen.width - (constants.PADDING * 2), app.screen.height - (constants.PADDING * 2));

    if (constants.GRID_DEBUG) {
        graphics.lineStyle(1, 0x000000, .1);

        let startPos, endPos;

        // draw vertical lines
        for (var i = 0; i <= map.size[0]; i++) {
            // console.log('vertical from', gridToPixel({x: i, y: 0}), 'to', gridToPixel({x: i, y: map.size[1]}));

            startPos = gridToPixel({x: i, y: 0});
            endPos = gridToPixel({x: i, y: map.size[1]});
            graphics.moveTo(startPos.x, startPos.y);
            graphics.lineTo(endPos.x, endPos.y);
        }

        // draw horizontal lines
        for (var i = 0; i <= map.size[1]; i++) {
            startPos = gridToPixel({x: 0, y: i});
            endPos = gridToPixel({x: map.size[0], y: i});
            graphics.moveTo(startPos.x, startPos.y);
            graphics.lineTo(endPos.x, endPos.y);
        }
    }
}


function gameLoop(delta) {

    // update state
    stateManager.updateActiveState(delta);

}
