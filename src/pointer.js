export default class PointerHelper {
  constructor(PIXI, element, scale = 1) {

    //Add element and scale properties
    this.element = element;
    this._scale = scale;
    //An array to store all the pointer objects
    //(there will usually just be one)
    this.pointers = [];

    //A local PIXI reference
    this.PIXI = PIXI;

  }

  get scale() {
    return this._scale;
  }

  set scale(value) {
    this._scale = value;

    //Update scale values for all pointers
    this.pointers.forEach(pointer => pointer.scale = value);
  }

  makePointer(element = this.element, scale = this.scale) {
    //The pointer object will be returned by this function
    let pointer = {
      element: element,
      _scale: scale,

      //Private x and y properties
      _x: 0,
      _y: 0,

      //Width and height
      width: 1,
      height: 1,

      //The public x and y properties are divided by the scale. If the
      //HTML element that the pointer is sensitive to (like the canvas)
      //is scaled up or down, you can change the `scale` value to
      //correct the pointer's position values
      get x() {
        return this._x / this.scale;
      },
      get y() {
        return this._y / this.scale;
      },

      //Add `centerX` and `centerY` getters so that we
      //can use the pointer's coordinates with easing
      //and collision functions
      get centerX() {
        return this.x;
      },
      get centerY() {
        return this.y;
      },

      //`position` returns an object with x and y properties that
      //contain the pointer's position
      get position() {
        return {
          x: this.x,
          y: this.y
        };
      },

      get scale() {
        return this._scale;
      },
      set scale(value) {
        this._scale = value;
      },

      //Add a `cursor` getter/setter to change the pointer's cursor
      //style. Values can be "pointer" (for a hand icon) or "auto" for
      //an ordinary arrow icon.
      get cursor() {
        return this.element.style.cursor;
      },
      set cursor(value) {
        this.element.style.cursor = value;
      },

      //Booleans to track the pointer state
      isDown: false,
      isUp: true,
      tapped: false,

      //Properties to help measure the time between up and down states
      downTime: 0,
      elapsedTime: 0,

      //Optional `press`,`release` and `tap` methods
      press: undefined,
      release: undefined,
      tap: undefined,

      onTapFns: [],
      onTap(fn) {
        this.onTapFns.push(fn);
      },

      //A property to check whether or not the pointer
      //is visible
      _visible: true,
      get visible() {
        return this._visible;
      },
      set visible(value) {
        if (value === true) {
          this.cursor = "auto";
        } else {
          this.cursor = "none";
        }
        this._visible = value;
      },

      //The pointer's mouse `moveHandler`
      moveHandler(event) {

        //Get the element that's firing the event
        let element = event.target;

        //Find the pointer’s x and y position (for mouse).
        //Subtract the element's top and left offset from the browser window
        this._x = (event.pageX - element.offsetLeft);
        this._y = (event.pageY - element.offsetTop);

        //Prevent the event's default behavior 
        event.preventDefault();
      },

      //The pointer's `touchmoveHandler`
      touchmoveHandler(event) {
        let element = event.target;

        //Find the touch point's x and y position
        this._x = (event.targetTouches[0].pageX - element.offsetLeft);
        this._y = (event.targetTouches[0].pageY - element.offsetTop);
        event.preventDefault();
      },

      //The pointer's `downHandler`
      downHandler(event) {
        //Set the down states
        this.isDown = true;
        this.isUp = false;
        this.tapped = false;

        //Capture the current time
        this.downTime = Date.now();

        //Call the `press` method if it's been assigned
        if (this.press) this.press();
        event.preventDefault();
      },

      //The pointer's `touchstartHandler`
      touchstartHandler(event) {
        let element = event.target;

        //Find the touch point's x and y position
        this._x = event.targetTouches[0].pageX - element.offsetLeft;
        this._y = event.targetTouches[0].pageY - element.offsetTop;

        //Set the down states
        this.isDown = true;
        this.isUp = false;
        this.tapped = false;

        //Capture the current time
        this.downTime = Date.now();

        //Call the `press` method if it's been assigned
        if (this.press) this.press();
        event.preventDefault();
      },

      //The pointer's `upHandler`
      upHandler(event) {
        //Figure out how much time the pointer has been down
        this.elapsedTime = Math.abs(this.downTime - Date.now());

        //If it's less than 200 milliseconds, it must be a tap or click
        if (this.elapsedTime <= 200 && this.tapped === false) {
          this.tapped = true;

          //Call the `tap` method if it's been assigned
          if (this.tap) this.tap();
          if (this.onTapFns.length) {
            this.onTapFns.forEach((fn) => { fn(); });
          }
        }
        this.isUp = true;
        this.isDown = false;

        //Call the `release` method if it's been assigned
        if (this.release) this.release();

        //`event.preventDefault();` needs to be disabled to prevent <input> range sliders
        //from getting trapped in Firefox (and possibly Safari)
        //event.preventDefault();
      },

      //The pointer's `touchendHandler`
      touchendHandler(event) {

        //Figure out how much time the pointer has been down
        this.elapsedTime = Math.abs(this.downTime - Date.now());

        //If it's less than 200 milliseconds, it must be a tap or click
        if (this.elapsedTime <= 200 && this.tapped === false) {
          this.tapped = true;

          //Call the `tap` method if it's been assigned
          if (this.tap) this.tap();
        }
        this.isUp = true;
        this.isDown = false;

        //Call the `release` method if it's been assigned
        if (this.release) this.release();
        //event.preventDefault();
      },

      //`hitTestSprite` figures out if the pointer is touching a sprite
      hitTestSprite(sprite) {

        //Add global `gx` and `gy` properties to the sprite if they
        //don't already exist
        addGlobalPositionProperties(sprite);

        //The `hit` variable will become `true` if the pointer is
        //touching the sprite and remain `false` if it isn't
        let hit = false;

        //Find out the sprite's offset from its anchor point
        let xAnchorOffset, yAnchorOffset;
        if (sprite.anchor !== undefined) {
          xAnchorOffset = sprite.width * sprite.anchor.x;
          yAnchorOffset = sprite.height * sprite.anchor.y;
        } else {
          xAnchorOffset = 0;
          yAnchorOffset = 0;
        }

        //Is the sprite rectangular?
        if (!sprite.circular) {

          //Get the position of the sprite's edges using global
          //coordinates
          let left = sprite.gx - xAnchorOffset,
            right = sprite.gx + sprite.width - xAnchorOffset,
            top = sprite.gy - yAnchorOffset,
            bottom = sprite.gy + sprite.height - yAnchorOffset;

          //Find out if the pointer is intersecting the rectangle.
          //`hit` will become `true` if the pointer is inside the
          //sprite's area
          hit = this.x > left && this.x < right && this.y > top && this.y < bottom;
        }

        //Is the sprite circular?
        else {
          //Find the distance between the pointer and the
          //center of the circle
          let vx = this.x - (sprite.gx + (sprite.width / 2) - xAnchorOffset),
            vy = this.y - (sprite.gy + (sprite.width / 2) - yAnchorOffset),
            distance = Math.sqrt(vx * vx + vy * vy);

          //The pointer is intersecting the circle if the
          //distance is less than the circle's radius
          hit = distance < sprite.width / 2;
        }
        //Check the value of `hit`
        return hit;
      }
    };

    //Bind the events to the handlers
    //Mouse events
    element.addEventListener(
      "mousemove", pointer.moveHandler.bind(pointer), false
    );
    element.addEventListener(
      "mousedown", pointer.downHandler.bind(pointer), false
    );

    //Add the `mouseup` event to the `window` to
    //catch a mouse button release outside of the canvas area
    window.addEventListener(
      "mouseup", pointer.upHandler.bind(pointer), false
    );

    //Touch events
    element.addEventListener(
      "touchmove", pointer.touchmoveHandler.bind(pointer), false
    );
    element.addEventListener(
      "touchstart", pointer.touchstartHandler.bind(pointer), false
    );

    //Add the `touchend` event to the `window` object to
    //catch a mouse button release outside of the canvas area
    window.addEventListener(
      "touchend", pointer.touchendHandler.bind(pointer), false
    );

    //Disable the default pan and zoom actions on the `canvas`
    element.style.touchAction = "none";

    //Add the pointer to Tink's global `pointers` array
    this.pointers.push(pointer);

    //Return the pointer
    return pointer;
  }
}
